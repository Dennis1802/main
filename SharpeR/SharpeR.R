# install.packages("SharpeR")
# install.packages("readxl")

library("readxl")
library("SharpeR")


# under the null
set.seed(1234)
# test <- matrix(rnorm(500*5),ncol=5)
# test <- data.matrix(df_returns)
# test <- data.matrix(df_returns[, c(1,6)])

df_returns <- read_excel("C:/Users/Eigenaar/OneDrive/QF/QF Thesis/main code/PGP/results/df_returns.xlsx", sheet = "Sheet2")

matrix <- matrix(0, nrow=1, ncol=10)

for (i in 1:10){
  rv <- sr_equality_test(data.matrix(df_returns[, c(10+i,50+i)])) # i=2 then take column 12 and 22
  matrix[i] <- rv$p.value
}
# --------------------------------------------------------------------------------

# --------------------------------------------------------------------------------
rv <- sr_equality_test(data.matrix(df_returns[, c(1,7)]))
rv$p.value
rv <- sr_equality_test(data.matrix(df_returns[, c(5,7)]))
rv$p.value
rv <- sr_equality_test(data.matrix(df_returns[, c(8,7)]))
rv$p.value

rv <- sr_equality_test(data.matrix(df_returns[, c(11,17)]))
rv$p.value
rv <- sr_equality_test(data.matrix(df_returns[, c(15,17)]))
rv$p.value
rv <- sr_equality_test(data.matrix(df_returns[, c(18,17)]))
rv$p.value

rv <- sr_equality_test(data.matrix(df_returns[, c(21,27)]))
rv$p.value
rv <- sr_equality_test(data.matrix(df_returns[, c(25,27)]))
rv$p.value
rv <- sr_equality_test(data.matrix(df_returns[, c(28,27)]))
rv$p.value

rv <- sr_equality_test(data.matrix(df_returns[, c(31,37)]))
rv$p.value
rv <- sr_equality_test(data.matrix(df_returns[, c(35,37)]))
rv$p.value
rv <- sr_equality_test(data.matrix(df_returns[, c(38,37)]))
rv$p.value

rv <- sr_equality_test(data.matrix(df_returns[, c(41,47)]))
rv$p.value
rv <- sr_equality_test(data.matrix(df_returns[, c(45,47)]))
rv$p.value
rv <- sr_equality_test(data.matrix(df_returns[, c(48,47)]))
rv$p.value

rv <- sr_equality_test(data.matrix(df_returns[, c(51,57)]))
rv$p.value
rv <- sr_equality_test(data.matrix(df_returns[, c(55,57)]))
rv$p.value
rv <- sr_equality_test(data.matrix(df_returns[, c(58,57)]))
rv$p.value
# --------------------------------------------------------------------------------

# --------------------------------------------------------------------------------
# rv <- sr_equality_test(data.matrix(df_returns[, c(1,9)]))
# rv$p.value
# rv <- sr_equality_test(data.matrix(df_returns[, c(5,9)]))
# rv$p.value
# rv <- sr_equality_test(data.matrix(df_returns[, c(8,9)]))
# rv$p.value
# 
# rv <- sr_equality_test(data.matrix(df_returns[, c(11,19)]))
# rv$p.value
# rv <- sr_equality_test(data.matrix(df_returns[, c(15,19)]))
# rv$p.value
# rv <- sr_equality_test(data.matrix(df_returns[, c(18,19)]))
# rv$p.value
# 
# rv <- sr_equality_test(data.matrix(df_returns[, c(21,29)]))
# rv$p.value
# rv <- sr_equality_test(data.matrix(df_returns[, c(25,29)]))
# rv$p.value
# rv <- sr_equality_test(data.matrix(df_returns[, c(28,29)]))
# rv$p.value
# 
# rv <- sr_equality_test(data.matrix(df_returns[, c(31,39)]))
# rv$p.value
# rv <- sr_equality_test(data.matrix(df_returns[, c(35,39)]))
# rv$p.value
# rv <- sr_equality_test(data.matrix(df_returns[, c(38,39)]))
# rv$p.value
# 
# rv <- sr_equality_test(data.matrix(df_returns[, c(41,49)]))
# rv$p.value
# rv <- sr_equality_test(data.matrix(df_returns[, c(45,49)]))
# rv$p.value
# rv <- sr_equality_test(data.matrix(df_returns[, c(48,49)]))
# rv$p.value
# 
# rv <- sr_equality_test(data.matrix(df_returns[, c(51,59)]))
# rv$p.value
# rv <- sr_equality_test(data.matrix(df_returns[, c(55,59)]))
# rv$p.value
# rv <- sr_equality_test(data.matrix(df_returns[, c(58,59)]))
# rv$p.value


# rv <- sr_equality_test(data.matrix(df_returns[, c(2,7)]))
# rv$p.value
# rv <- sr_equality_test(data.matrix(df_returns[, c(3,8)]))
# rv$p.value
# rv <- sr_equality_test(data.matrix(df_returns[, c(4,9)]))
# rv$p.value
# 
# rv <- sr_equality_test(data.matrix(df_returns[, c(12,17)]))
# rv$p.value
# rv <- sr_equality_test(data.matrix(df_returns[, c(13,18)]))
# rv$p.value
# rv <- sr_equality_test(data.matrix(df_returns[, c(14,19)]))
# rv$p.value
# 
# rv <- sr_equality_test(data.matrix(df_returns[, c(22,27)]))
# rv$p.value
# rv <- sr_equality_test(data.matrix(df_returns[, c(23,28)]))
# rv$p.value
# rv <- sr_equality_test(data.matrix(df_returns[, c(24,29)]))
# rv$p.value
# 
# rv <- sr_equality_test(data.matrix(df_returns[, c(32,37)]))
# rv$p.value
# rv <- sr_equality_test(data.matrix(df_returns[, c(33,38)]))
# rv$p.value
# rv <- sr_equality_test(data.matrix(df_returns[, c(34,39)]))
# rv$p.value
# 
# rv <- sr_equality_test(data.matrix(df_returns[, c(42,47)]))
# rv$p.value
# rv <- sr_equality_test(data.matrix(df_returns[, c(43,48)]))
# rv$p.value
# rv <- sr_equality_test(data.matrix(df_returns[, c(44,49)]))
# rv$p.value
# 
# rv <- sr_equality_test(data.matrix(df_returns[, c(52,57)]))
# rv$p.value
# rv <- sr_equality_test(data.matrix(df_returns[, c(53,58)]))
# rv$p.value
# rv <- sr_equality_test(data.matrix(df_returns[, c(54,59)]))
# rv$p.value
# --------------------------------------------------------------------------------


# # under the alternative (but with identity covariance)
# ope <- 253
# nyr <- 10
# nco <- 5
# set.seed(909)
# rets <- 0.01 * sapply(seq(0,1.7/sqrt(ope),length.out=nco),
#                       function(mu) { rnorm(ope*nyr,mean=mu,sd=1) })
# rv <- sr_equality_test(rets)

