import pandas as pd
import rpy2.robjects as ro
from rpy2.robjects.packages import importr
import rpy2.robjects as robjects
from rpy2.robjects import pandas2ri

from rpy2.robjects.conversion import localconverter

# import R's package
base = importr('base')
PerformanceAnalytics = importr('PerformanceAnalytics')
vroom = importr('vroom')

df = pd.read_csv("C:/Users/Eigenaar/OneDrive/QF/QF Thesis/main code/PGP/moment estimates/log_returns_factors.csv")
df.drop(['Unnamed: 0'], axis=1, inplace=True)


# Defining the R script and loading the instance in Python
r = robjects.r
r['source']('MultivariateMoments.R')

# Loading the function we have defined in R.
m2ewma = robjects.globalenv['M2.ewma']

# Reading and processing data
df = pd.read_csv("Country-Sales.csv")

#converting it into r object for passing into r function
df_r = pandas2ri.ri2py(df)
#Invoking the R function and getting the result
df_result_r = filter_country_function_r(df_r, 'USA')
#Converting it back to a pandas dataframe.
df_result = pandas2ri.py2ri(df_result_r)


# ewma
# https://www.askpython.com/python/examples/r-in-python
# 

M2.ewma = robjects.r['M2.ewma']
M3.ewma = robjects.r['M3.ewma']
M4.ewma = robjects.r['M4.ewma']

with localconverter(ro.default_converter + pandas2ri.converter):
  r_df = ro.conversion.py2rpy(df)

r_matrix = robjects.r.matrix(r_df, nrow=6)

import rpy2.robjects.numpy2ri
rpy2.robjects.numpy2ri.activate()

m2 = M2.ewma(r_matrix, 0.97)

m3 <- M3.ewma(df, lambda = 0.97)
m4 <- M4.ewma(df, lambda = 0.97)

robjects.globalenv['ewma']
