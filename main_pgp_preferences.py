import os

import numpy as np
import pandas as pd
import pytz
import pickle
from scipy import stats
from datetime import datetime

from portfolio_performance_measure import *

from qstrader.alpha_model.fixed_signals import FixedSignalsAlphaModel
# from qstrader.alpha_model.sma_signals import MovingAverageCrossStrategy
# from qstrader.alpha_model.ewma_signals import ExponentialWeightedMovingAverageCrossStrategy
# from qstrader.alpha_model.pgp_signals_rolling import PolynomialGoalProgrammingStrategy 
from qstrader.alpha_model.pgp_utility_signals_rolling import PolynomialGoalProgrammingStrategy #1

from qstrader.signals.sma import SMASignal
from qstrader.risk_model.simple_risk_model import SimpleRiskModel
# from qstrader.risk_model.hmm_risk_model import RegimeHMMRiskManager
# from qstrader.risk_model.hmm_risk_model_factor import RegimeHMMRiskManager
from qstrader.risk_model.hmm_risk_model_factor import RegimeHMMRiskManager

from qstrader.asset.equity import Equity
from qstrader.asset.universe.static import StaticUniverse
from qstrader.data.backtest_data_handler import BacktestDataHandler
from qstrader.data.daily_bar_csv import CSVDailyBarDataSource
from qstrader.statistics.tearsheet import TearsheetStatistics
# from qstrader.statistics.tearsheet_one_year import TearsheetStatisticsSmall
from qstrader.trading.backtest import BacktestTradingSession


#%#%
# perform moments estimations R
# re estimate on a rolling basis -> prbb not daily...but monthly/quarterly/annually -> monthly is oke
# re perform for both alpha and risk model

# use multiple or singular risk model -> beware regimes 0/1 switch ... -> multiple causes overfitting?

# given equity curve, which performance measures to show?
# run for all preferences and moments

# how do transaction costs affect the performance with risk model

#%%

if __name__ == "__main__":
    # start_dt = pd.Timestamp('2021-03-20 14:30:00', tz=pytz.UTC)
    # end_dt = pd.Timestamp('2021-04-30 23:59:00', tz=pytz.UTC)
    
    # start_dt = pd.Timestamp('1993-08-02 14:30:00', tz=pytz.UTC)
    start_dt = pd.Timestamp('1993-07-30 14:30:00', tz=pytz.UTC) #2
    end_dt = pd.Timestamp('2021-04-30 23:59:00', tz=pytz.UTC)

    # Construct the symbol and asset necessary for the backtest
    strategy_symbols = [
                        # 'GLD',
                        'Mkt-RF','SMB','HML','RMW','CMA','MOM',
                        # 'SPY',
                        'SHV',
                        ]
    
    strategy_assets = ['EQ:%s' % symbol for symbol in strategy_symbols]
    strategy_universe = StaticUniverse(strategy_assets)

    # To avoid loading all CSV files in the directory, set the
    # data source to load only those provided symbols
    DEFAULT_CONFIG_FILENAME = 'C:/Users/Eigenaar/OneDrive/QF/QF Thesis/main code/data/'
    csv_dir = os.environ.get('QSTRADER_CSV_DATA_DIR', DEFAULT_CONFIG_FILENAME)
    data_source = CSVDailyBarDataSource(csv_dir, Equity, csv_symbols=strategy_symbols)
    data_handler = BacktestDataHandler(strategy_universe, data_sources=[data_source])

    # Construct an Alpha Model that simply provides a fixed
    # signal for the single GLD ETF at 100% allocation
    # with a backtest that does not rebalance
    # strategy_alpha_model = FixedSignalsAlphaModel({
    #                                                 'EQ:Mkt-RF': 1/6,
    #                                                 'EQ:SMB': 1/6,
    #                                                 'EQ:HML': 1/6,
    #                                                 'EQ:RMW': 1/6,
    #                                                 'EQ:CMA': 1/6,
    #                                                 'EQ:MOM': 1/6,
    #                                                 'EQ:SHV': 0.0,
    #                                                 # 'EQ:SPY': 1.0
    #                                                 # 'EQ:GLD': 1.0,
    #                                               })
    # strategy_alpha_model = ExponentialWeightedMovingAverageCrossStrategy({'EQ:SPY': 0.5,'EQ:SHV': 0.5}, data_handler=data_handler)
    
    # annualize data 
    annualize = 252
    method = 'ewma' #3!
    
    # step 2 results df
    portfolios = list('ABCDEFGHIJ') #portfolios = list(string.ascii_uppercase[:10]) 
    preferences = ((1,0,0,0),(1,1,0,0),(1,1,1,0),(1,1,0,1),(1,1,1,1),
            (10,0,0,0),(1,10,0,0),(1,1,10,0),(1,1,0,10),(10,10,10,10)) 
    
    x0 = tuple([1/len(strategy_symbols)] * len(strategy_symbols))
    data = {pf: preferences[i]+x0 for i, pf in enumerate(portfolios)}
    lst = ['l1','l2','l3','l4']
    # moments = ['Mean','Volatility','Skewness','Kurtosis']
    
    df_allocation = pd.DataFrame(data, index = lst + list(strategy_symbols))
    
    # out-of-sample results df
    portfolios = list('ABCDEFGHIJ')
    moments = ['Mean','Volatility','Skewness','Kurtosis', 'Cum. Return']
    benchmarks = ['Sharpe','Sortino','Stutzer','Omega']
    
    df_performance = pd.DataFrame(np.nan, columns=portfolios, index=moments+benchmarks)
    
    # step 2 results check df
    df_bool = pd.DataFrame(np.nan, columns = portfolios, index = list(range(0,328)))
    
    # equity curve df
    df_equity_curve = pd.DataFrame(np.nan, columns = portfolios, index = list(range(0,7241)))
    
    # HIT ratio df
    df_hit_ratio = pd.DataFrame(np.nan, columns = portfolios, index = list(range(0,1)))
    
    np.random.seed(0)
    for i, preference in enumerate(preferences): 
        
        strategy_alpha_model = PolynomialGoalProgrammingStrategy({
                                                                  'EQ:Mkt-RF': 1/6,
                                                                  'EQ:SMB': 1/6,
                                                                  'EQ:HML': 1/6,
                                                                  'EQ:RMW': 1/6,
                                                                  'EQ:CMA': 1/6,
                                                                  'EQ:MOM': 1/6,
                                                                  'EQ:SHV': 0.0,
                                                                  # 'EQ:SPY': 1.0,
                                                                  # 'EQ:GLD': 1.0,
                                                                  }, data_handler=data_handler,
                                                                     method=method,preference=preference,
                                                                     epsilon=0.01,iter=10000)
        
        # strategy_risk_model = SimpleRiskModel({'EQ:GLD': 1.0}) # weights overwritten in pcm class
        # pickle_path = DEFAULT_CONFIG_FILENAME + "/hmm_model_factors.pkl"
        # hmm_model = pickle.load(open(pickle_path, "rb"))
        strategy_risk_model = RegimeHMMRiskManager(data_handler=data_handler) 
        
        # strategy_signals = SMASignal(start_dt, strategy_universe, [10])
        strategy_backtest = BacktestTradingSession(
            start_dt,
            end_dt,
            strategy_universe,
            strategy_alpha_model,
            # strategy_risk_model, #4!
            # signals = strategy_signals,  
            # initial_cash=1e6,
            rebalance='end_of_month',
            # rebalance_weekday='WED',
            long_only=True,
            cash_buffer_percentage=0.01,
            data_handler=data_handler
        )
        strategy_backtest.run()
        
        # # Construct benchmark assets (buy & hold SPY)
        # benchmark_symbols = ['SPY']
        # benchmark_assets = ['EQ:SPY']
        # benchmark_universe = StaticUniverse(benchmark_assets)
        # benchmark_data_source = CSVDailyBarDataSource(csv_dir, Equity, csv_symbols=benchmark_symbols)
        # benchmark_data_handler = BacktestDataHandler(benchmark_universe, data_sources=[benchmark_data_source])
    
        # # Construct a benchmark Alpha Model that provides
        # # 100% static allocation to the SPY ETF, with no rebalance
        # benchmark_alpha_model = FixedSignalsAlphaModel({'EQ:SPY': 1.0})
        # benchmark_backtest = BacktestTradingSession(
        #     start_dt,
        #     end_dt,
        #     benchmark_universe,
        #     benchmark_alpha_model,
        #     rebalance='buy_and_hold', # If strategy daily then benchmark also has to be daily
        #     long_only=True,
        #     cash_buffer_percentage=0.01,
        #     data_handler=benchmark_data_handler
        # )
        # benchmark_backtest.run()
         
        # Performance Output
        tearsheet = TearsheetStatistics(
            strategy_equity=strategy_backtest.get_equity_curve(),
            # benchmark_equity=benchmark_backtest.get_equity_curve(),
            title='Dynamic PGP Factors Without HMM - EOM rebalancing/1e5/{}/{}'.format(method,preference), #5!
            # title='Dynamic PGP Factors With HMM (Mkt 1-factor) - EOM rebalancing/{}/{}'.format(method,preference),
            rolling_sharpe=True
        )
        tearsheet.plot_results()
#%%
        # step 2 results
        weight = strategy_backtest.qts.portfolio_construction_model.optimiser.weight
        weight = pd.DataFrame(weight).T
        df_allocation[portfolios[i]][4:4+len(strategy_symbols)] = weight.mean()
        
        # step 2 results check
        df_bool.index = strategy_backtest.rebalance_schedule
        opt_result = strategy_backtest.alpha_model.opt_result
    
        for key in opt_result:
            df_bool[portfolios[i]][key] = opt_result[key]['success']
        
        # equity curve df
        df_equity_curve.index = strategy_backtest.get_equity_curve().index
        df_equity_curve[portfolios[i]] = strategy_backtest.get_equity_curve()
        
#%%
        # out-of-sample results
        strategy_equity=strategy_backtest.get_equity_curve()
        
        # portfolio statistics
        strategy_equity['Return'] = strategy_equity['Equity'].pct_change(1)
        cum_ret = strategy_equity['Equity'][-1]/strategy_equity['Equity'][0]-1
        df_performance[portfolios[i]][4] = cum_ret 
        #print('Our return {} was percent'.format(cum_ret))
        x = stats.describe(strategy_equity['Return'].dropna())
        df_performance[portfolios[i]][0] = 100*annualize*x[2] #annualized mean in %
        df_performance[portfolios[i]][1] = 100*np.sqrt(annualize*x[3]) #annualized volatility in %
        df_performance[portfolios[i]][2:4] = np.array(x[4:])
        #strategy_equity['Return'].plot(kind='kde')
        
        # portfolio benchmarks (yearly)
        ret,vol,sharpe = sharpe_ratio(returns=strategy_equity['Return'], 
                                    rfr=0, annualize=annualize)
        ret,down_vol,sortino = sortino_ratio(returns=strategy_equity['Return'], 
                                           target=0, rfr=0, annualize=annualize)
        stutzer = stutzer_ratio(returns=strategy_equity['Return'], 
                                           target=0, rfr=0, annualize=annualize)
        ret,omega_vol,omega = omega_ratio(returns=strategy_equity['Return'], 
                                           target=0, rfr=0, annualize=annualize)
        
        df_performance[portfolios[i]][5] = sharpe
        df_performance[portfolios[i]][6] = sortino
        df_performance[portfolios[i]][7] = stutzer
        df_performance[portfolios[i]][8] = omega
#%%
        # # compute hit ratio #6!
        # rebalance_schedule = strategy_backtest.rebalance_schedule
        # df = strategy_backtest.risk_model.df
        # counter = 0
        # for num, dt in enumerate(rebalance_schedule):
        #     j = strategy_equity.index.get_loc(dt.date()) + 1
        #     if j == 7241:
        #         break
        #     while j != strategy_equity.index.get_loc(rebalance_schedule[num+1].date()): #!
        #         if (df[dt]['regimes'][dt] == 0 and np.sign(strategy_equity['Return'].iloc[j]) == 1) or \
        #             (df[dt]['regimes'][dt] == 0 and np.sign(strategy_equity['Return'].iloc[j]) == 0) or \
        #             (df[dt]['regimes'][dt] == 1 and np.sign(strategy_equity['Return'].iloc[j]) == 0) or \
        #             (df[dt]['regimes'][dt] == 1 and np.sign(strategy_equity['Return'].iloc[j]) == -1):
        #             counter = counter + 1
        #         j = j + 1 #!
                
        # df_hit_ratio[portfolios[i]] = counter/7240 # ! 328/7240/6988
#%%
# step 1 results  
opt = pd.DataFrame(strategy_backtest.alpha_model.opt).T
opt_mean = opt.mean()
df_opt = strategy_backtest.alpha_model.df_opt
df_opt_mean = sum(df_opt.values()) / len(df_opt)

# equity curve returns df
df_returns = df_equity_curve.pct_change(1)
 
#%%
# import qstrader.statistics.performance as perf
# strategy_equity=strategy_backtest.get_equity_curve()
# stats = tearsheet.get_results(strategy_equity)

# returns = stats["returns"]
# cum_returns = stats['cum_returns']

# cagr = perf.create_cagr(cum_returns)
# sharpe = perf.create_sharpe_ratio(returns)
# sortino = perf.create_sortino_ratio(returns)
# dd, dd_max, dd_dur = perf.create_drawdowns(cum_returns)


# df = pd.DataFrame(['{:.2%}'.format(cagr), 
#                    '{:.2f}'.format(sharpe),
#                    '{:.2f}'.format(sortino),
#                    '{:.2%}'.format(returns.std() * np.sqrt(252)),
#                    '{:.2%}'.format(dd_max),
#                    '{:.0f}'.format(dd_dur)], index= ['CAGR', 
#                                                      'Sharpe Ratio', 
#                                                      'Sortino Ratio', 
#                                                      'Annual Volatility',
#                                                      'Max Daily Drawdown',
#                                                      'Max Drawdown Duration (Days)'])
                                                     
#%%
# # rebalance schedule
# rebalance_schedule = strategy_backtest.rebalance_schedule

# # alpha model
# signal_weights_alpha = strategy_backtest.alpha_model.signal_weights
# epsilon = strategy_backtest.alpha_model.epsilon
# iter = strategy_backtest.alpha_model.iter
# # weight = strategy_backtest.alpha_model.weight
# # weight = pd.DataFrame(weight).T
# df_adj_close_prices = strategy_backtest.alpha_model.df_adj_close_prices
# df_adj_close_returns = strategy_backtest.alpha_model.df_adj_close_returns
# # df = strategy_backtest.alpha_model.df

# mean = strategy_backtest.alpha_model.mean
# cov = strategy_backtest.alpha_model.cov
# skew = strategy_backtest.alpha_model.skew
# kurt = strategy_backtest.alpha_model.kurt
# opt = strategy_backtest.alpha_model.opt
# df_opt = strategy_backtest.alpha_model.df_opt
# opt_result = strategy_backtest.alpha_model.opt_result
# # weights=opt_result.x[4:]

# # risk model
# signal_weights_risk = strategy_backtest.risk_model.signal_weights
# hmm_model = strategy_backtest.risk_model.hmm_model
# adj_close_prices = strategy_backtest.risk_model.adj_close_prices
# adj_close_returns = strategy_backtest.risk_model.adj_close_returns
# # df_adj_close_prices = strategy_backtest.risk_model.df_adj_close_prices # multiple factor
# # df_adj_close_returns = strategy_backtest.risk_model.df_adj_close_returns # multiple factor
# df = strategy_backtest.risk_model.df
# weight = strategy_backtest.risk_model.weight
# weight = pd.DataFrame(weight).T

# # from matplotlib import cm, pyplot as plt
# # import seaborn as sns
# # sns.set(palette = 'bright',color_codes=True)

# # for factor in weight.columns:
# #     # plt.plot(weight.loc[start_dt:end_dt])
# #     plt.plot(weight[factor], label=factor)

# # plt.legend()

# # portfolio construction
# adj_close_returns = strategy_backtest.qts.portfolio_construction_model.optimiser.adj_close_returns
# weight = strategy_backtest.qts.portfolio_construction_model.optimiser.weight
# weight = pd.DataFrame(weight).T
# # weight.columns = strategy_symbols

# # pcm methods
# dt = pd.Timestamp('2022-12-30 21:00:00', tz=pytz.UTC)
# optimised_weights = strategy_backtest.qts.portfolio_construction_model.optimiser(dt, initial_weights=weight)
# full_assets = strategy_backtest.qts.portfolio_construction_model._obtain_full_asset_list(dt)
# full_zero_weights = strategy_backtest.qts.portfolio_construction_model._create_zero_target_weight_vector(full_assets)
# full_weights = strategy_backtest.qts.portfolio_construction_model._create_full_asset_weight_vector(full_zero_weights, optimised_weights)

# target_portfolio =  strategy_backtest.qts.portfolio_construction_model._generate_target_portfolio(dt, full_weights)
# current_portfolio = strategy_backtest.qts.portfolio_construction_model._obtain_current_portfolio()
# rebalance_orders = strategy_backtest.qts.portfolio_construction_model._generate_rebalance_orders(dt, target_portfolio, current_portfolio)

#%%
# # order sizer
# order_sizer = strategy_backtest.qts.portfolio_construction_model.order_sizer

# # execution handler
# strategy_backtest.qts.execution_handler(dt, rebalance_orders)

#%%
# # broker init
# account_name = 'Backtest Simulated Broker Account'
# portfolio_id = '000001'
# portfolio_name = 'Backtest Simulated Broker Portfolio'
# account_id = strategy_backtest.broker.account_id
# base_currency = strategy_backtest.broker.base_currency
# initial_funds = strategy_backtest.broker.initial_funds
# cash_balances = strategy_backtest.broker.cash_balances
# portfolios = strategy_backtest.broker.portfolios

# # broker methods
# strategy_backtest.broker.subscribe_funds_to_account(7)
# strategy_backtest.broker.withdraw_funds_from_account(4)
# strategy_backtest.broker.get_account_cash_balance()

# strategy_backtest.broker.get_account_total_market_value()
# strategy_backtest.broker.get_account_total_equity() # cash and assets
# strategy_backtest.broker.list_all_portfolios()

# # broker methods on portfolio
# strategy_backtest.broker.get_portfolio_as_dict(portfolio_id)
# strategy_backtest.broker.get_portfolio_cash_balance(portfolio_id)
# strategy_backtest.broker.get_portfolio_total_equity(portfolio_id)

# # portfolio init
# strategy_backtest.broker.list_all_portfolios()[0].name
# strategy_backtest.broker.list_all_portfolios()[0].cash
# history = strategy_backtest.broker.list_all_portfolios()[0].history
# strategy_backtest.broker.portfolios[portfolio_id].portfolio_to_dict()

# # position handler init
# strategy_backtest.broker.portfolios[portfolio_id].pos_handler

# strategy_backtest.broker.portfolios[portfolio_id].pos_handler.positions    

#%%

# # To avoid bullshit
# weight = {}
# signal_weights = {}
# signal_weights['EQ:SPY'] = 1.23
# signal_weights['EQ:SHV'] = 0
# dt = pd.Timestamp('2010-5-26 21:00:00', tz=pytz.UTC)
# dt = pd.Timestamp('2010-5-27 21:00:00', tz=pytz.UTC)
# weight[dt] = signal_weights


# # daily rebalance holiday adjusted
# key = list(data_handler.data_sources[0].asset_bar_frames.keys())[0]
# adj_close_prices = data_handler.data_sources[0].asset_bar_frames[key]['Adj Close']

# market_time = "21:00:00"
# rebalance_dates = pd.bdate_range(
#     start=start_dt, end=end_dt,
# )
# rebalance_times = [
#     pd.Timestamp(
#         "%s %s" % (date, market_time), tz=pytz.utc
#     )
#     for date in rebalance_dates
# ]

# # Holiday adjusted rebalance_times
# for i in rebalance_times:
#     if i not in adj_close_prices.index:
#         rebalance_times.remove(i) 

# if pd.Timestamp('2001-9-12 21:00:00', tz=pytz.UTC) in rebalance_times:
#     rebalance_times.remove(pd.Timestamp('2001-9-12 21:00:00', tz=pytz.UTC))
# if pd.Timestamp('2001-9-14 21:00:00', tz=pytz.UTC) in rebalance_times:
#     rebalance_times.remove(pd.Timestamp('2001-9-14 21:00:00', tz=pytz.UTC))
# if pd.Timestamp('2007-1-2 21:00:00', tz=pytz.UTC) in rebalance_times:
#     rebalance_times.remove(pd.Timestamp('2007-1-2 21:00:00', tz=pytz.UTC))
# if pd.Timestamp('2012-10-30 21:00:00', tz=pytz.UTC) in rebalance_times:
#     rebalance_times.remove(pd.Timestamp('2012-10-30 21:00:00', tz=pytz.UTC))
    
