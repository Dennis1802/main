# get_data
import os as os
import sys as sys 
# path = os.path.dirname(os.path.realpath('__file__'))
path = 'C:/Users/Eigenaar/OneDrive/QF/QF Thesis/main code/data/'

import numpy as np
import pandas as pd
import pandas_datareader as pdr


start_date = "1970-12-5"
# end_date = "2021-01-05"

# strategy_symbols = ['BABA',
#                     'SHV',
#                     'SPLG',
#                     'ISFA.AS',
#                     'AMRN',
#                     'DIS',
#                     'ATVI',
#                     'VAR1.DE',
#                     'FLOW.AS',
#                     'CSCO',
#                     'PYPL',
#                     'SPXU']

strategy_symbols = ['SPY','QQQ','GLD'] # SHV partially created

# Obtain the adjusted closing prices from Yahoo finance
for i, symbol in enumerate(strategy_symbols):
    df = pdr.get_data_yahoo(strategy_symbols[i], start_date)
    df.to_csv(path + symbol + '.csv')
    
    