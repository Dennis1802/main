# -*- coding: utf-8 -*-
"""
portfolio benchmarks 
- substract rfr at the start
- hold assets with large skew and kurtosis, preferably only for short-
to mid-term

- Gold Section method or Nelder-Mead? =? lecture 3 simplex LP -> difference
"""

import os as os
import sys as sys 
path = os.path.dirname(os.path.realpath('__file__'))
#path = os.path.realpath('//campus.eur.nl/users/students/447283xh/Documents')
import numpy as np
import pandas as pd
import math
from scipy.optimize import minimize

# http://www.turingfinance.com/computational-investing-with-python-week-one/
# https://blog.quantinsti.com/portfolio-management-strategy-python/
# https://medium.com/@mariano.scandizzo/strategic-asset-allocation-with-python-c9afef392e90


# measures of risk-adjusted return based on volatility
# https://www.quantnews.com/performance-metrics-sharpe-ratio-sortino-ratio/
# https://ad1m.github.io/derivative/financial_metrics.html
def treynor_ratio(returns, benchmark, rfr, annualize):
    """
    - https://www.investopedia.com/terms/t/treynorratio.asp
    """
    ret = returns.mean()
    cov = np.cov(returns,benchmark)[1][1]
    var = np.var(benchmark)
    beta = cov/var
    treynor = np.sqrt(annualize)*(ret - rfr)/beta
    return np.array([ret,beta,treynor])

def sharpe_ratio(returns, rfr, annualize):
    """
    Takes in weights, returns array or return, volatility, sharpe ratio
    - could be any distribution, but preferably a symmetrical distribution
    - does not account for skewness or kurtosis
    - measure of historical performance and assumes future market conditions would be similar
    - large positive returns are penalized just as much as large negative returns
    """
    ret = returns.mean()
    vol = returns.std()
    sharpe = np.sqrt(annualize)*(ret - rfr)/vol
    
    return np.array([ret, vol, sharpe])


# measures of risk-adjusted return based on partial moments

def lpm(returns, threshold, order):
    # This method returns a lower partial moment of the returns
    # Create an array he same length as returns containing the minimum return threshold
    threshold_array = np.empty(len(returns))
    threshold_array.fill(threshold)
    # Calculate the difference between the threshold and the returns
    diff = threshold_array - returns
    # Set the minimum of each to 0
    diff = np.array(diff.fillna(0)).clip(min=0)
    # Return the sum of the different to the power of order
    return np.sum(diff ** order) / len(returns)

# target=0 -> Rollinger and Hoffman (2013)
def sortino_ratio(returns, target, rfr, annualize):
    """
    Penalizes only for negative returns
    """
    ret = returns.mean()
    #down_vol = returns[returns<target].std() -> meh but why
    #down_vol = math.sqrt(np.sum(returns[returns<target]**2) / len(returns)) -> meh as target
    down_vol = math.sqrt(lpm(returns, target, 2))
    sortino = np.sqrt(annualize)*(ret - rfr)/down_vol
    return np.array([ret,down_vol,sortino])

def omega_ratio(returns, target, rfr, annualize):
    """
    - https://breakingdownfinance.com/finance-topics/performance-measurement/omega-ratio/
    - https://quantdare.com/omega-ratio-the-ultimate-risk-reward-ratio/
    """
    ret = returns.mean()
    #omega_vol = np.sum(-returns[returns<target])/ len(returns)
    omega_vol = lpm(returns, target, 1) 
    omega = np.sqrt(annualize)*(ret - rfr)/omega_vol
    return np.array([ret,omega_vol,omega])

def kappa_ratio(returns, target, rfr, annualize):
    """
    
    """
    ret = returns.mean()
    #kappa_vol = math.pow(np.sum(-returns[returns<target]**3)/ len(returns), float(1/3)) 
    kappa_vol = math.pow(lpm(returns, target, 3), float(1/3)) 
    kappa = np.sqrt(annualize)*(ret - rfr)/kappa_vol
    return np.array([ret,kappa_vol,kappa])

def stutzer_ratio(returns, target, rfr, annualize):
    """
    - http://investexcel.net/stutzer-index/
    - https://papers.ssrn.com/sol3/papers.cfm?abstract_id=239540
    """
    ret = returns.mean()

    def MLE(theta):
        return np.log(1/len(returns)*np.sum(np.exp(theta*returns)))
        
    guess = [0]
    opt_result = minimize(MLE, guess, method='Nelder-Mead', 
                          options={'disp': True})
    I = -opt_result.fun
    stutzer = abs(ret)/ret * np.sqrt(2*I)
    
    return stutzer

