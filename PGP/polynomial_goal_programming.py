# -*- coding: utf-8 -*-
"""
polynomial goal programming
- rewrite max to min problem - i.e. rewrite objective function 'only'
- is the optimization problem smooth and convex? -> nonsmooth and nonconvex due
to inclusion of higher moments such as skew and kurtosis
-> if convex, local optimal = global optimal. If non-convex, then it may have 
multiples solutions with a global optimum/infeasible solution/objective function unbounded
- in PGP feasible solutions always exists, hence an optimal solution can also 
be found (Kemalbay et al. (2010)) 
- choose correct algorithm (searching local or global optima?) 
-> shgo/differential evolution not SLSQP
- if you still encounter problems, can in still be solved in another manner?
"""


import os as os
path = os.path.dirname(os.path.realpath('__file__'))
from coskew import coskew
from cokurt import cokurt
from scipy.optimize import minimize, LinearConstraint, NonlinearConstraint, Bounds
from scipy.optimize import shgo
from scipy.optimize import differential_evolution
#import scipy
#print(scipy.__version__)
import pandas as pd
import numpy as np
import datetime

# https://scipy-lectures.org/advanced/mathematical_optimization/index.html
# Python: https://towardsdatascience.com/building-a-higher-dimensional-efficient-frontier-in-python-ce66ca2eff7
# Matlab: http://www.quantatrisk.com/2013/01/20/coskewness-and-cokurtosis/

d={}
def polynomial_goal_programming_1(df,m1,m2,m3,m4,epsilon,iter):
    
    # if np.all(pd.isna(m2)):
    #     m2 = df.cov()
        
    # Step 1: Mathematical Optimization of Four Subproblems
    def obj_mean(weights):
        return -(np.sum(m1 * weights))
    
    def obj_var(weights):
        return np.dot(weights.T, np.dot(m2, weights))   
    
    def obj_skew(weights):
        return -(np.dot(weights.T, np.dot(m3,np.kron(weights,weights))))
    
    def obj_kurt(weights):
        return np.dot(weights.T, np.dot(m4,np.kron(weights,np.kron(weights,weights))))
    
    # 0-1 bounds for each weight -> no short selling
    bounds = [(0, 1),] * len(df.columns) #float('inf')
    #bounds = Bounds([0.]* len(df.columns), [1.]* len(df.columns))
    
    # Constraints
    lc = LinearConstraint(np.ones(len(df.columns)), lb=1-epsilon, ub=1)
     
    # Differential Evolution
    d['opt_mean'] = differential_evolution(obj_mean,bounds=bounds,maxiter=iter,constraints=(lc))
    d['opt_var'] = differential_evolution(obj_var,bounds=bounds,maxiter=iter,constraints=(lc))
    d['opt_skew'] = differential_evolution(obj_skew,bounds=bounds,maxiter=iter,constraints=(lc))
    d['opt_kurt'] = differential_evolution(obj_kurt,bounds=bounds,maxiter=iter,constraints=(lc))
    opt = (-d['opt_mean'].fun, d['opt_var'].fun, -d['opt_skew'].fun, d['opt_kurt'].fun)
    
    data_opt = {moment: d[moment].x for i, moment in enumerate(d)}
    df_opt = np.round(pd.DataFrame(data_opt, index=list(df.columns)),3)
    
    return m1, m2, m3, m4, opt, df_opt

def polynomial_goal_programming_slsqp(l0,df,m1,m2,m3,m4,opt,guess):
    # Step 2: PGP
    args = opt + l0
    
    def obj(z0, a0,a1,a2,a3,a4,a5,a6,a7):
        return np.power(1+np.abs(z0[0]/a0),a4) \
                + np.power(1+np.abs(z0[1]/a1),a5) \
                + np.power(1+np.abs(z0[2]/a2),a6) \
                + np.power(1+np.abs(z0[3]/a3),a7)
                
    def constraint1(z0, a0,a1,a2,a3,a4,a5,a6,a7):
        weights = z0[4:]
        return np.sum(m1 * weights) + z0[0] - a0
    
    def constraint2(z0, a0,a1,a2,a3,a4,a5,a6,a7):
        weights = z0[4:]
        return np.dot(weights.T, np.dot(m2, weights)) - z0[1] - a1
    
    def constraint3(z0, a0,a1,a2,a3,a4,a5,a6,a7):
        weights = z0[4:]
        return np.dot(weights.T, np.dot(m3,np.kron(weights,weights))) + z0[2] - a2
    
    def constraint4(z0, a0,a1,a2,a3,a4,a5,a6,a7):
        weights = z0[4:]
        return np.dot(weights.T, np.dot(m4,np.kron(weights,np.kron(weights,weights)))) - z0[3] - a3
    
    def check_sum(z0, a0,a1,a2,a3,a4,a5,a6,a7):    
    
        '''
        Returns 0 if sum of weights is 1.0
        '''
        weights = z0[4:]
        return np.sum(weights) - 1
    
    # Initial Guess Goal and Weight Variables
    d0 = [0]*4
    x0 = list(guess) #[1/len(df.columns)] * len(df.columns) 
    
    # 0-1 bounds for each weight -> no short selling
    d0_bnds = [(0, 50)] * 4 #float('inf') 
    bounds = [(0, 1),] * len(df.columns) #float('inf')
    bnds = d0_bnds + bounds
    
    con1 = {'type':'eq','fun': constraint1, 'args':args}
    con2 = {'type':'eq','fun': constraint2, 'args':args}
    con3 = {'type':'eq','fun': constraint3, 'args':args}
    con4 = {'type':'eq','fun': constraint4, 'args':args}
    cons = {'type':'eq','fun': check_sum, 'args':args}
    
    multi_cons = (con1, con2, con3, con4, cons)
    opt_result = minimize(obj,d0+x0,args=args,method='SLSQP',bounds=bnds,constraints=multi_cons)
    #opt_result = shgo(obj,bounds=bnds,args=args,constraints=multi_cons,n=1,iters=1,options={maxtime:1}) 

    return opt_result

def polynomial_goal_programming_2(l0,df,m1,m2,m3,m4,opt,epsilon,iter):
    
    # Step 2 PGP:
    args = opt + l0
    
    def obj(z0, a0,a1,a2,a3,a4,a5,a6,a7):
        return np.power(np.abs(z0[0]/a0),a4) \
                + np.power(np.abs(z0[1]/a1),a5) \
                + np.power(np.abs(z0[2]/a2),a6) \
                + np.power(np.abs(z0[3]/a3),a7)
                
    def constraint1(z0):
        weights = z0[4:]
        return np.sum(m1 * weights) + z0[0]
    
    def constraint2(z0):
        weights = z0[4:]
        return np.dot(weights.T, np.dot(m2, weights)) - z0[1]
    
    def constraint3(z0):
        weights = z0[4:]
        return np.dot(weights.T, np.dot(m3,np.kron(weights,weights))) + z0[2]
    
    def constraint4(z0):
        weights = z0[4:]
        return np.dot(weights.T, np.dot(m4,np.kron(weights,np.kron(weights,weights)))) - z0[3]
     
    # =============================================================================
    #     def check_sum(z0):
    #         '''
    #         Returns 0 if sum of weights is 1.0
    #         '''
    #         weights = z0[4:]
    #         return np.sum(weights)
    # =============================================================================
    
    # assumption that goal variables will not exceed 50
    d0_bnds = [(0, 50),] * 4 #float('inf') 
    bounds = [(0, 1),] * len(df.columns)
    bnds = d0_bnds + bounds
    
    nlc1 = NonlinearConstraint(constraint1, lb=opt[0], ub=opt[0])
    nlc2 = NonlinearConstraint(constraint2, lb=opt[1], ub=opt[1])
    nlc3 = NonlinearConstraint(constraint3, lb=opt[2], ub=opt[2])
    nlc4 = NonlinearConstraint(constraint4, lb=opt[3], ub=opt[3])
    #nlc = NonlinearConstraint(check_sum, lb=1-epsilon, ub=1)
    lc = LinearConstraint(np.append(np.zeros(4),np.ones(len(df.columns))), lb=1-epsilon, ub=1)
    
    multi_cons = (nlc1, nlc2, nlc3, nlc4, lc)
    opt_result = differential_evolution(obj,bounds=bnds,args=args,maxiter=iter,constraints=multi_cons)
    
    return opt_result

