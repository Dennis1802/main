# -*- coding: utf-8 -*-
"""
market invariant tests
https://medium.com/engineer-quant/optimal-portfolios-why-and-how-be0cdcf16485
https://medium.com/engineer-quant/market-invariants-can-there-really-be-predictive-power-in-market-data-bb4999dbe75e
"""

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt


def hist_test(stock_data):
  
    ret1 = stock_data.iloc[1:len(stock_data)//2]
    ret2 = stock_data.iloc[(len(stock_data)//2)+1:]
    #ret1 = np.log(data1) - np.log(data1.shift(1))
    #ret2 = np.log(data2) - np.log(data2.shift(1))
    plt.figure(1)
    ax1 = plt.subplot(2, 1, 1)
    plt.hist(ret1, bins=40)
    plt.title(stock_data.name + " Log Factor Returns Histogram")
    plt.ylabel("Frequency")
    ax2 = plt.subplot(2, 1, 2, sharex=ax1)
    plt.hist(ret2, bins=40)
    plt.xlabel("Log Factor Returns")
    plt.ylabel("Frequency")
    plt.show()

def scatter_test(stock_data):
    ret1 = stock_data.iloc[1:len(stock_data)//2]
    ret2 = stock_data.iloc[(len(stock_data)//2)+1:] #same size -> +2
    #ret1 = np.log(data1) - np.log(data1.shift(1))
    #ret2 = np.log(data2) - np.log(data2.shift(1))
    plt.scatter(ret1, ret2)
    plt.title(stock_data.name + " Scatter Plot")
    plt.xlabel("Log Factor Returns subset 1")
    plt.ylabel("Log Factor Returns subset 2")
    plt.show() 

