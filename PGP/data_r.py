# -*- coding: utf-8 -*-
"""
Data R
"""

import os as os
import sys as sys 
path = os.path.dirname(os.path.realpath('__file__'))
import numpy as np
import pandas as pd

def data_r(df,file=None,method=None):
    
    # try:
    #     m2_file = 'moment estimates/' + file + '_m2_' + method + '.csv'
    #     m2 = pd.read_csv(os.path.join(path, m2_file),sep=';').to_numpy()
    # except OSError:
    #     m2 = pd.DataFrame(np.nan, columns=df.columns, index=range(len(df.columns)))
        
    m2_file = 'moment estimates/' + file + '_m2_' + method + '.csv'
    m2 = pd.read_csv(os.path.join(path, m2_file),sep=',').to_numpy()
    m3_file = 'moment estimates/' + file + '_m3_' + method + '.csv'
    m3 = pd.read_csv(os.path.join(path, m3_file),sep=',').to_numpy()
    m4_file = 'moment estimates/' + file + '_m4_' + method + '.csv'
    m4 = pd.read_csv(os.path.join(path, m4_file),sep=',').to_numpy() 
    
    return m2, m3, m4

