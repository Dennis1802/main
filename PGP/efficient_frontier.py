# -*- coding: utf-8 -*-
"""
plotting efficient frontier
- adjust returns~but monthly returns: sum over assets and/or sum over time
"""

import os as os
import sys as sys 
path = os.path.dirname(os.path.realpath('__file__'))
#path = os.path.realpath('//campus.eur.nl/users/students/447283xh/Documents')
from coskew import coskew
from cokurt import cokurt
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
sns.set(palette = 'bright',color_codes=True)


def monte_carlo_sim(df, num_ports, annualize):  
    
    m3 = coskew(df)
    m4 = cokurt(df)
    
    np.random.seed(10)
    trials = num_ports
    all_weights = np.zeros((trials, len(df.columns)))
    
    ret_arr = np.zeros(trials)
    vol_arr = np.zeros(trials)
    coskew_arr = np.zeros(trials)
    cokurt_arr = np.zeros(trials)

    for i in range(trials):
        
        # Get random weights and store in weight array
        weights = np.array(np.random.random(len(df.columns)))
        weights = weights/np.sum(weights)
        all_weights[i,:] = weights

        # Expected return
        ret_arr[i] = np.sum((df.mean()*weights)*annualize)

        # Volatility
        vol_arr[i] = np.sqrt(np.dot(weights.T, np.dot(df.cov()*annualize, weights)))

        # Coskew
        coskew_arr[i] = np.dot(weights.T, np.dot(m3,np.kron(weights,weights)))
        
        #Cokurtosis
        cokurt_arr[i] = np.dot(weights.T, np.dot(m4,np.kron(weights,np.kron(weights,weights))))

    return vol_arr, ret_arr, cokurt_arr, coskew_arr

# Plot Efficient Frontier
def efficient_frontier_plot(vol_arr, ret_arr,  cokurt_arr, coskew_arr):
    
    fig = plt.figure()
    ax = plt.axes(projection='3d')
    a = ax.scatter(vol_arr, cokurt_arr, ret_arr, c=coskew_arr, cmap='winter')
    a = fig.colorbar(a, label = 'Skew')
    ax.set_xlabel('Volatility')
    ax.set_ylabel('Kurtosis')
    ax.set_zlabel('Return')

# Plot Efficient Frontier with cmap
def efficient_frontier_cmap(vol_arr, ret_arr, arr, label):
    
    plt.figure()
    plt.scatter(vol_arr, ret_arr, c=arr, cmap='winter')
    plt.colorbar(label=label)
    plt.xlabel('Volatility')
    plt.ylabel('Return')

