# -*- coding: utf-8 -*-
"""
data
"""

import os as os
import sys as sys 
path = os.path.dirname(os.path.realpath('__file__'))
import numpy as np
import pandas as pd

# https://investmentcache.com/magic-of-log-returns-practical-part-2/
# https://medium.com/engineer-quant/optimal-portfolios-why-and-how-be0cdcf16485


def data(sector):
    
    if sector:
        industry_file = '12_Industry_Portfolios.csv'
        df1 = pd.read_csv(os.path.join(path, industry_file), sep=',') 
        df1['date'] = pd.date_range(start='7/1/1963', end='1/1/2020', freq='MS')
        df1.set_index('date', inplace=True)
        df1.drop(['Unnamed: 0'], axis=1, inplace=True)
        
        factor_file = 'factors.csv'
        df2 = pd.read_csv(os.path.join(path, factor_file), sep=',') 
        df2['date'] = pd.date_range(start='7/1/1963', end='1/1/2020', freq='MS')
        df2.set_index('date', inplace=True)
        
        rf = df2['RF']
        df = df1.sub(rf, axis=0)
        
        df = np.log(1+df/100)

        return df
    
    else:
        factor_file = 'factors.csv'
        df2 = pd.read_csv(os.path.join(path, factor_file), sep=',') 
        df2['date'] = pd.date_range(start='7/1/1963', end='1/1/2020', freq='MS')
        df2.set_index('date', inplace=True)
        df2.drop(['Unnamed: 0','RF','IntRate'], axis=1, inplace=True)
        
        df = np.log(1+df2/100)
    
        return df
    
        