# -*- coding: utf-8 -*-
"""
create cokurt matrix
- why is mtx1 not used

https://towardsdatascience.com/building-a-higher-dimensional-efficient-frontier-in-python-ce66ca2eff7
https://stackoverflow.com/questions/41890870/how-to-calculate-coskew-and-cokurtosis
"""

import numpy as np


def cokurt(df): 
    
    # Number of stocks
    num = len(df.columns)
    
    #First Tensor Product Matrix
    mtx1 = np.zeros(shape = (len(df), num**2))
    
    #Second Tensor Product Matrix
    mtx2 = np.zeros(shape = (len(df), num**3))
    
    v = df.to_numpy()
    means = v.mean(0,keepdims=True)
    v1 = (v-means).T

    for k in range(num):
        for i in range(num):
            for j in range(num):
                    vals = v1[i]*v1[j]*v1[k]
                    #mtx2[:,(k*(num**2))+(i*num)+j] = vals/float((len(df)-1) \
                    #* df.iloc[:,i].std()*df.iloc[:,j].std()*df.iloc[:,k].std())
                    mtx2[:,(k*(num**2))+(i*num)+j] = vals/float((len(df)-1))

    m4 = np.dot(v1,mtx2)
    for i in range(num**3):
        use = i%num
        #m4[:,i] = m4[:,i]/float(df.iloc[:,use].std())
        m4[:,i] = m4[:,i]
        
    return m4