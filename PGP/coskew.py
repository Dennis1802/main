# -*- coding: utf-8 -*-
"""
create coskew matrix

https://towardsdatascience.com/building-a-higher-dimensional-efficient-frontier-in-python-ce66ca2eff7
https://stackoverflow.com/questions/41890870/how-to-calculate-coskew-and-cokurtosis
"""

import numpy as np


def coskew(df):
    
    # Number of stocks
    num = len(df.columns)
    
    # Two dimionsal matrix for tensor product 
    mtx = np.zeros(shape = (len(df), num**2))
    
    v = df.to_numpy()
    means = v.mean(0,keepdims=True)
    v1 = (v-means).T
    
    for i in range(num):
        for j in range(num):
                vals = v1[i]*v1[j]
                #mtx[:,(i*num)+j] = vals/float((len(df)-1)*df.iloc[:,i].std() \
                #* df.iloc[:,j].std())
                mtx[:,(i*num)+j] = vals/float((len(df)-1))
    
    #coskewness matrix
    m3 = np.dot(v1,mtx)
    
    #Normalize by dividing by standard deviation
    for i in range(num**2):
        use = i%num
        #m3[:,i] = m3[:,i]/float(df.iloc[:,use].std())
        m3[:,i] = m3[:,i]
    return m3