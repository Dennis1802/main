# -*- coding: utf-8 -*-
"""
"Past performance is no guarantee of future results." -> which assumptions have
changed?

understanding past/current/future environment -> moment estimations -> 
asset allocation based on preferences -> past performance results

portfolio optimization assumptions

Data
- dataset: value-weighted 12 industries and 17 factors
- monthly factor returns in % -> in sample: 1963/7-1993/7; out-of-sample: 1993/8-2020/1 
 
! annual return mult 12 for ef -> check/article -> switching

Statistics
- excess log industry returns/log factor returns -> check time invariance

Moment Estimations: nonparametric/ewma/structured/shrinkage
-> ewma/shrink show lots of potential -> shrinkage performs better with less data
-> assumes market conditions to stay constant: the past predicts the future
-> portfolio allocation is based on the moment estimates -> R other moments

! more and better estimates of the four moments
-> analysis of prior crises/change in (look-aliked data) and out-of-sample period/
specify distribution/choose different criteria/lambda
-> regime switching models (nonstationary time series) or ewma

PGP
- invest amount between (0,1) with sum is 1 -> no-short selling
- eps=0.01 and 10k iter
- as differential evolution is a stochastic algorithm that searches large areas 
for candidate solutions, it is not guaranteed that a solution can be found
- SLSQP searches for local

-> model other asset allocation models
-> improve pgp model -> +1 makes sense (enthropy) - moment constraints/other constraints
-> implement better optimization methods
-> utilize different portfolios


performance measures
- incorporate more measures -> Information Ratio, drawdown, VaR, ES, 1/N, S&P

big picture
- More refined -> different time intervals/better understanding of data or context 
+ regime switching/dynamic models/scenarios/DAA/TAA + long/short
- Past is not predictor of future returns -> factors are?
- DAA/TAA/ewma/scenario’s/factors
- robustness test

-> dynamic model estimates? -> PerformanceAnalytics Package
-> switch dates + check before/after allocation (self-fulfilling propheciency?) 
"""

#%%
"""
Improvements
-	Smoothing stocks just like macroeconomic variables to remove noise + correct data biases + simulate data: bootstrapping and mcmc/ross
-	Combined stocks/bond portfolio or other assets/portfolios

Additional ideas or details
-	Markov-Switching model/early warning model/cb/non-return factor /shocks/diversified financials
	Endogeneity/var -> self-fulfilling proficiency/cointegration/dynamic factor-garch -> extensions possible?
-	Bayes/Shrinkage (e.g. ridge/lasso) – (Koijen et at. (2017) or methods such as Shrinkage (Bryzgalova 2015)
	Black-litterman + Garlappi’s paper
-	ML approach to the high dimensionality problem/ML/DL/sentiment/thesis plan + other papers
-	Cochrane + van de Wel -> macro and finance should be linked -> van dijk paper. Take note: distinguish between return and non-return factors (which can link financial markets to the rest of the economy) 
	robeco 5 papers/business cycles/others
-	Momentum + TAA + quantlib

1ST TR EXCHANGENASDAQ BK ETF
-	Why do some stocks tumble faster compared to others -> profit taking and margin calls 
	And reverse and re-reverse day-to-day?
-	For whom is the etf especially designed, and who are the largest holders

-	 Consider other relevant factors/thoughts/ideas

Inspiration
-	QF methods: Factor model with GARCH + slides lecture 8
-	Advanced Econometrics: VAR or VECM? 
-	quora 
"""

#%%
import os as os
import sys as sys 
path = os.path.dirname(os.path.realpath('__file__'))
#path = os.path.realpath('//campus.eur.nl/users/students/447283xh/Documents')
# from data import data
from data_statistics import data_statistics
from efficient_frontier import *
from market_invariant import *
from data_r import data_r
from portfolio_allocation import *
from polynomial_goal_programming import *
from portfolio_performance_measure import *
import numpy as np
import pandas as pd
import pandas_market_calendars as mcal
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import seaborn as sns
sns.set(palette = 'bright',color_codes=True)
from scipy import stats
from statsmodels.tsa import stattools
import string

# annualize data
annualize = 252

# change dates - in-sample: 1963/7-1993/7; out-of-sample: 1993/8-2020/1 
# substract risk free rate (!)
factor_file = 'factors.csv'   
df2 = pd.read_csv(os.path.join(path, factor_file), sep=',')

nyse = mcal.get_calendar('NYSE')
# nyse.valid_days(start_date='1963-07-01', end_date='2021-04-30')
schedule = nyse.schedule(start_date='1963-07-01', end_date='2021-04-30')
df2.set_index(schedule.index, inplace=True)
# df2.set_index(pd.to_datetime(schedule.index,utc=True), inplace=True)
# df2.rename(lambda dt: dt.replace(hour=21, minute=0, second=0), inplace=True)
df2.drop(['Unnamed: 0','RF'], axis=1, inplace=True)

df_copy = np.log(df2/100+1)
df = df_copy[:'1993-07-30'] #Test: '1990-08-01':'1993-07-01'
# df = df_copy[:'1978-08-10']
# df = df_copy['1978-08-11':'1993-07-30']

# # save file
# df.to_csv(path  + '/moment estimates/' + 'log_returns_factors.csv')

#%%
# # investment of 1 and reinvestments each month (!)
# df2_cum_ret = (df2/100+1).expanding().apply(np.prod,raw=True)

# # save data
# for i in df2_cum_ret.columns:
#     df = pd.DataFrame(df2_cum_ret[i], columns = ['Open', 'High', 'Low', 'Close', 'Volume', i])
#     df.index.name = 'Date'
#     df.rename(columns={i: 'Adj Close'}, inplace=True)
#     df.to_csv('C:/Users/Eigenaar/OneDrive/QF/QF Thesis/main code/data/' + i + '.csv')

#%%
# # dataframe statistics 
# df.plot(subplots=True, figsize=(20,12))
# df.describe().transpose()
# df.hist(bins=100, figsize=(12,8))
# plt.tight_layout()
# df.mean()
# df.var()
# df.skew()
# df.kurtosis()
# df.cov()
# df.corr()

# # data statistics - mean and variance annualized
# df_stats = data_statistics(df=df, annualize=annualize)

# # # portfolio simulations and efficient frontier plots
# # vol_arr, ret_arr, cokurt_arr, coskew_arr = monte_carlo_sim(df, num_ports=10000,
# #                                                            annualize=annualize)
# # efficient_frontier_plot(vol_arr, ret_arr, cokurt_arr, coskew_arr)
# # efficient_frontier_cmap(vol_arr, ret_arr, cokurt_arr, 'Kurtosis')
# x
# # market invariant tests
# for i in df:
#     hist_test(df[i])
#     scatter_test(df[i])
    
#%%
# gather m1, m2, m3 and m4 from R -> create R object in Python and perform task
m1 = df.mean()
m2, m3, m4 = data_r(df=df,file='factors',method='sample')

#m2_compare = df.ewm(alpha=0.03,min_periods=2,adjust=False,axis=0).cov()

# ewma = False
# if ewma:
#     m1 = df.ewm(alpha=0.03,min_periods=len(df),adjust=False,axis=0).mean().dropna().iloc[-1]
#     #df = df.ewm(alpha=0.03,min_periods=1,adjust=False,axis=0).mean()
# else:
#     m1 = df.mean()
    
    
import time
start = time.time()

# polynomial goal programming - step 1 -> eps = 0.001 doesnt improve much/2.5x longer
m1, m2, m3, m4, opt, df_opt = polynomial_goal_programming_1(df,m1,m2,m3,m4,epsilon=0.01,iter=10000)

end = time.time()
print(end - start)

#%%
start = time.time()
# polynomial goal programming -  step 2
portfolios = list('ABCDEFGHIJ') #portfolios = list(string.ascii_uppercase[:10]) 
preferences = ((1,0,0,0),(1,1,0,0),(1,1,1,0),(1,1,0,1),(1,1,1,1),
        (10,0,0,0),(1,10,0,0),(1,1,10,0),(1,1,0,10),(10,10,10,10)) 

x0 = tuple([1/len(df.columns)] * len(df.columns) + [0]*4)
data = {pf: preferences[i]+x0 for i, pf in enumerate(portfolios)}
lst = ['l1','l2','l3','l4']
moments = ['Mean','Volatility','Skewness','Kurtosis']

df_allocation = pd.DataFrame(data, index = lst + list(df.columns) + moments)
guesses = 2*([df_opt[x] for x in df_opt] + [df_opt['opt_mean']])

d={}
for i, preference in enumerate(preferences):
    opt_result = polynomial_goal_programming_slsqp(preference,df,m1,m2,m3,m4,opt,guesses[i])
    d[portfolios[i]] = opt_result
    df_allocation[portfolios[i]][4:4+len(df.columns)] = opt_result.x[4:]
    
    portfolio_val = portfolio_allocation(df=df, weights=opt_result.x[4:], 
                                     investment=1)
    portfolio_val['Monthly Return'] = portfolio_val['Total Pos'].pct_change(1)
    
    x = stats.describe(portfolio_val['Monthly Return'].dropna())
    df_allocation[portfolios[i]][-4] = 100*annualize*x[2] #annualize
    df_allocation[portfolios[i]][-3] = 100*np.sqrt(annualize*x[3]) #annualize
    df_allocation[portfolios[i]][-2:] = np.array(x[4:])
    
end = time.time()
print(end - start)

opt = pd.DataFrame(opt).T
d_bool = [d[key]['success'] for key in d]
#%%
df = df_copy['1993-08-01':]

# portfolio performance with out-of-sample
portfolios = list('ABCDEFGHIJ')
moments = ['Mean','Volatility','Skewness','Kurtosis', 'Cum. Return']
benchmarks = ['Sharpe','Sortino','Stutzer','Omega']

df_performance = pd.DataFrame(np.nan, columns=portfolios, index=moments+benchmarks)

for portfolio in df_performance:
    weights = df_allocation[portfolio][4:4+len(df.columns)]
    
    # portfolio allocation
    portfolio_val = portfolio_allocation(df=df, weights=weights, investment=1)
    
    # portfolio statistics
    portfolio_val['Monthly Return'] = portfolio_val['Total Pos'].pct_change(1)
    cum_ret = portfolio_val['Total Pos'][-1]/portfolio_val['Total Pos'][0]-1
    df_performance[portfolio][4] = cum_ret 
    #print('Our return {} was percent'.format(cum_ret))
    x = stats.describe(portfolio_val['Monthly Return'].dropna())
    df_performance[portfolio][0] = 100*annualize*x[2] #annualized in %
    df_performance[portfolio][1] = 100*np.sqrt(annualize*x[3]) #annualized in %
    df_performance[portfolio][2:4] = np.array(x[4:])
    #portfolio_val['Monthly Return'].plot(kind='kde')
    
    # portfolio benchmarks (yearly)
    ret,vol,sharpe = sharpe_ratio(returns=portfolio_val['Monthly Return'], 
                                rfr=0, annualize=annualize)
    ret,down_vol,sortino = sortino_ratio(returns=portfolio_val['Monthly Return'], 
                                       target=0, rfr=0, annualize=annualize)
    stutzer = stutzer_ratio(returns=portfolio_val['Monthly Return'], 
                                       target=0, rfr=0, annualize=annualize)
    ret,omega_vol,omega = omega_ratio(returns=portfolio_val['Monthly Return'], 
                                       target=0, rfr=0, annualize=annualize)
    
    df_performance[portfolio][5] = sharpe
    df_performance[portfolio][6] = sortino
    df_performance[portfolio][7] = stutzer
    df_performance[portfolio][8] = omega
    
    