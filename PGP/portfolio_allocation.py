# -*- coding: utf-8 -*-
"""
portfolio allocation
"""

import os as os
import sys as sys 
path = os.path.dirname(os.path.realpath('__file__'))
#path = os.path.realpath('//campus.eur.nl/users/students/447283xh/Documents')
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

# store dataframes in dictionary
d={}

def portfolio_allocation(df, weights, investment):
    
    # normalize returns
    for factor_df in df.columns:
        d[factor_df] = pd.DataFrame(df[factor_df])
        d[factor_df]['Normed Return'] = 1.00
        
        for i in range(1, len(df)):
             d[factor_df]['Normed Return'].iat[i] = \
             np.exp(np.sum(d[factor_df][factor_df][:i]))
    
    # allocations from portfolio optimization
    for factor_df,allo in zip(df.columns,weights):
        d[factor_df]['Allocation'] = d[factor_df]['Normed Return']*allo
    
    # investment 
    for factor_df in df.columns:
        d[factor_df]['Position Values'] = d[factor_df]['Allocation']*investment
    
    # total portfolio value
    pos = [d[factor_df]['Position Values'] for factor_df in df.columns]
    portfolio_val = pd.concat(pos, axis=1)
    portfolio_val.columns = list(df.columns)
    
    portfolio_val['Total Pos'] = portfolio_val.sum(axis=1)
    
    # plot
    portfolio_val['Total Pos'].plot(figsize=(10,8))
    plt.title('Total Portfolio Value')
    portfolio_val.drop('Total Pos', axis=1).plot(figsize=(10,8), kind='line')
    
    return portfolio_val

