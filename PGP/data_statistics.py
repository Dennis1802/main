# -*- coding: utf-8 -*-
"""
data statistics
- mean and variance annualized
"""

import os as os
import sys as sys
path = os.path.dirname(os.path.realpath('__file__'))
from scipy import stats
from statsmodels.tsa import stattools
import numpy as np
import pandas as pd


def data_statistics(df, annualize):
    x = stats.describe(df)
    statistics = {'Mean': 100*annualize*x[2], 'Volatility': 100*np.sqrt(annualize*x[3]), 
                  'Skewness': x[4], 'Kurtosis': x[5]}

    df_stats = pd.DataFrame(statistics, columns = ['Mean', 'Volatility', 
                                                   'Skewness', 'Kurtosis', 
                                                   'jb_value', 'p', 'ar1', 
                                                   'ar2', 'ar3'], index = df.columns)
    ar0 = 0
    for i,industry in enumerate(df.columns):
        df_stats['jb_value'].iat[i], df_stats['p'].iat[i] = stats.jarque_bera(df[industry])
        
        ar0, df_stats['ar1'].iat[i], df_stats['ar2'].iat[i], df_stats['ar3'].iat[i] = stattools.acf(df[industry], fft=True, nlags=3)
        
    return df_stats

